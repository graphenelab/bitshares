export const ru = {
    "global": {
        "tbd": "В разработке"
    },
    "dashboard": {
        "title": "Дэшборд"
    },
    "exchange": {
        "title": "Обмен"
    },
    "assets": {
        "title": "Мои ассеты"
    },
    "blockchain": {
        "title": "Блекчейн"
    },
    "voting": {
        "title": "Голосование"
    },
    "business": {
        "title": "Бизнес"
    },
    "settings": {
        "title": "Настройки"
    },
    "contacts": {
        "title": "Контакты"
    },
    "help": {
        "title": "Помощь"
    },
    "general": {
        "title": "Основные"
    },
    "wallet": {
        "title": "Кошелек"
    },
    "security": {
        "title": "Защита"
    },
    "nodes": {
        "title": "Ноды"
    },
    "backup": {
        "title": "Бэкапы и восстановление"
    }
};
